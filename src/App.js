import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: []
    }
  };

  componentDidMount() {
    let url = "http://localhost:3001/posts"
    fetch(url)
      .then(resp => resp.json())
      .then(data => {
        let posts = data.map((posts, index) => {
          return (
            <div key="{index}">
              <h3>{posts.title}</h3>
              <p>{posts.text}</p>
            </div>
          )
        })
        this.setState({ posts: posts })
      })
  }
  render() {
    return (
      <div className="app">
        {this.state.posts}
      </div>
    )
  };
}
export default App;
